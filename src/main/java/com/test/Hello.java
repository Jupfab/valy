package com.test;

import jakarta.validation.Valid;
import jakarta.validation.Validation;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.logging.Level;
import java.util.logging.Logger;

class A {
    @NotBlank
    String x;
}

class B {

    //@NotNull    // Because @Valid will skip null references
    @Valid
    A a;

    @NotBlank
    String y;
}

public class Hello {
    static Logger log = Logger.getAnonymousLogger();
    public static void main(String[] args) {
        log.log(Level.INFO, "Test");

        var factory = Validation.buildDefaultValidatorFactory();
        var validator = factory.getValidator();

        B b = new B();
        b.y = "Hello";
        //b.a = new A();


        var violations = validator.validate(b);

        for(var violation : violations) {
            log.log(Level.SEVERE, violation.getPropertyPath() + " " + violation.getMessage());
        }
    }
}
